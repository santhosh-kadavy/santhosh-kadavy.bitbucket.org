	var LoginView=Backbone.View.extend({
	el:'#mainDiv',
	template:_.template($('#loginTemplate').html()),
	initialize:function(){
		this.$el.empty();
		this.render();
	},
	render:function(){
		return this.$el.append(this.template);
	},
	events:{
		'click button#loginBtn':'login'
	},
	login:function(){
		var userId=this.$('#userId').val();
		var pwd=this.$('#pwd').val();
		var loginUser=users.find(function(user){
			return user.get('emailId')==userId && user.get('password')==pwd
		});
		if(!_.isUndefined(loginUser)){
			new HomeView({model:loginUser});
		}else{
			$('#errorMsg').empty();
			$('#errorMsg').append("Invalid emailId or password");
		}

	}
});

var HomeView=Backbone.View.extend({
	el:'#mainDiv',
	template:_.template($('#homeTemplate').html()),
	initialize:function(){
		this.$el.empty();
		this.render();
		this.renderInnerView();
	},
	render:function(){
		return this.$el.append(this.template);
		
	},
	renderInnerView:function(){
		new NavView();
		new TweetPageView({model:this.model});
	}
});

var NavView=Backbone.View.extend({
	el:'#navDiv',
	template:_.template($('#navTemplate').html()),
	initialize:function(){
		this.render();
	},
	render:function(){

		return this.$el.append(this.template);
		
	}
});

var TweetPageView=Backbone.View.extend({
	el:'#tweetPageDiv',
	template:_.template($('#tweetPageTemplate').html()),
	initialize:function(){
		this.render();
		this.renderInnerView();
	},
	render:function(){
		return this.$el.append(this.template());
	},
	renderInnerView:function(){
		new LoginUserView({model:this.model});
		new PostTweetView({model:this.model});
		new TweetListView({collection:messages});
		new FollowListView({model:this.model});
	}
});

var LoginUserView=Backbone.View.extend({
	el:'#loginUserDiv',
	template:_.template($('#loginUserTemplate').html()),
	initialize:function(){
		this.render();
	},
	render:function(){
		return this.$el.append(this.template({'loginUsr':this.model.toJSON()}));
	}
});

var PostTweetView=Backbone.View.extend({
	el:'#postTweetDiv',
	template:_.template($('#postTweetTemplate').html()),
	initialize:function(){
		this.render();
	},
	render:function(){
		return this.$el.append(this.template());
	},
	events:{
		"click button#tweet":"postTweet"
	},
	postTweet:function(){
		var text=$('#tweetText').val();
		var tweetModel=new MessageModel();
		tweetModel.set({"messageId":messages.length+1,"image":"","userId":this.model.get('emailId'),"text":text,"noOfLikes":0});
		messages.unshift(tweetModel);
		new TweetListView({collection:messages});
	}
});

var TweetView=Backbone.View.extend({
	template:_.template($('#tweetTemplate').html()),
	render:function(){
		this.$el.append(this.template({'tweet':this.model}));
		return this.$el;
	},
	events:{
		"click .glyphicon-heart":"updateLikes"
	},
	updateLikes:function(){
		this.model.set("noOfLikes",this.model.get("noOfLikes")+1);
	}
});

var TweetListView=Backbone.View.extend({
	el:'#tweetListDiv',
	initialize:function(){
		this.collection.on("change:noOfLikes",this.render,this);
		this.render();
	},
	render:function(){
		var self=this;
		this.$el.empty();
		this.collection.each(function(tweet){
			var tweetView=new TweetView({model:tweet});
			self.$el.append(tweetView.render());
		});
		
		return self.$el;
		
	}
});

var SignUpView=Backbone.View.extend({
	el:'#mainDiv',
	template:_.template($('#signUpTemplate').html()),
	initialize:function(){
		this.$el.empty();
		this.render();
	},
	render:function(){
		return this.$el.append(this.template());
	},
	events:{
		"click button#signUpBtn":"signup"
	},
	signup: function(){
		var userModel=new UserModel();
		userModel.set({"firstName":$('#firstName').val(),"lastName":$('#lastName').val(),"emailId":$('#emailId').val(),"password":$('#password').val(),"dp":$('#imgPath').val()});
		users.add(userModel);
		$('#successMsg').empty();
		$('#successMsg').append("Successfully registered   "+"<a href='#'>Go back to login page</a>");
	}
});

var FollowListView=Backbone.View.extend({
	el:'#followList',
	template:_.template($('#followTemplate').html()),
	initialize:function(){
		this.render();
	},
	render:function(){
		var loginUserModel=this.model;
		var followUserCollection=users.reject(function(user){
			return user.get('emailId')==loginUserModel.get('emailId');
		});
		followUserCollection=new UserCollection(followUserCollection);
		return this.$el.append(this.template({'followUsers':followUserCollection}));
	}
	
});