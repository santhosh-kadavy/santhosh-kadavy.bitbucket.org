var UserModel=Backbone.Model.extend({
	defaults:{
		firstName:'',
		lastName:'',
		password:'',
		dp:'',
		emailId:''
	}
});

var MessageModel=Backbone.Model.extend({
	idAttribute:'messageId',
	defaults:{
		messageId:0,
		emailId:null,
		text:'',
		image:'',
		timeStamp:''
	}
});


var UserCollection=Backbone.Collection.extend({
	model:UserModel
});

var MessageCollection=Backbone.Collection.extend({
	model:MessageModel
});

var SignUpRouter=Backbone.Router.extend({
	routes:{
		"":"login",
		"signup":"goToSignUp",
		"logout":"logout"
	},
	goToSignUp:function (){
		new SignUpView();
	},
	login:function(){
		new LoginView();
	},
	logout:function(){
		new LoginView();
	}
});


