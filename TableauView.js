
var IndexView=Backbone.View.extend({
	el:'#indexDiv',
	template:_.template($('#indexTmpl').html()),
	initialize:function(){
		this.render();
	},
	render:function(){
		return this.$el.append(this.template);
	},
	events:{
		'click #signIn':'signin'
	},
	signin:function(){
		this.remove();
		new LoginView();

	}
});

var LoginView=Backbone.View.extend({
	el:'#loginDiv',
	template:_.template($('#loginTmpl').html()),
	initialize:function(){
		this.render();
	},
	render:function(){
		return this.$el.append(this.template);
	},events:{
		'click #loginBtn':'login'
	},
	login:function(){
		this.remove();
		new ProjectsView();

	}
});

var ProjectsView=Backbone.View.extend({
	el:'#projectDiv',
	template:_.template($('#projectTmpl').html()),
	initialize:function(){
		this.render();
	},
	render:function(){
		return this.$el.append(this.template);
	}
});