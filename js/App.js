
var users=[{"name":"Alex","gender":"male","dp":"male.jpg"},{"name":"Tina","gender":"female","dp":"female.jpg"},{"name":"Vishnu","gender":"male","dp":"male.jpg"},{"name":"John","gender":"male.jpg","dp":"male.jpg"},{"name":"Saleem","gender":"male.jpg","dp":"male.jpg"},{"name":"Arjun","gender":"male.jpg","dp":"male.jpg"},{"name":"Swathi","gender":"female","dp":"female.jpg"},{"name":"Martin","gender":"male.jpg","dp":"male.jpg"},{"name":"Remo","gender":"male.jpg","dp":"male.jpg"},{"name":"Rose","gender":"female","dp":"female.jpg"}];
var messages=[{"text":"Participated in Hyderabad marathon","image":"Starfish.jpg"},{"text":"Back to school","image":""},{"text":"Watching India vs Australia at Chinnaswamy stadium","image":""},{"text":"Awesome climate here","image":"Sunset.jpg"},{"text":"Health is wealth","image":"Owl.jpg"},{"text":"Customer is always right","image":"Parachute.jpg"},{"text":"Eat dinner like beggar","image":""},{"text":"Drink 2 litres of water per day","image":""},{"text":"Use public transportation for daily commute","image":""},{"text":"Push yourself beyond limits","image":""},{"text":"Serve the poor","image":"Angrybirds.jpg"},{"text":"Patience is a virtue","image":"Marble.jpg"},{"text":"Family comes first","image":""},{"text":"Prevention is better than cure","image":""},{"text":"Live free or die hard","image":"Squirrel.jpg"},{"text":"Thumbs up, taste the thunder","image":""},{"text":"Happy women's day","image":""},{"text":"Got engaged","image":""},{"text":"Congrats to the Mangalyan team","image":"Hill.jpg"},{"text":"Go cashless","image":""}];
var loginUser={"name":"Santhosh Kadavy","gender":"male","dp":"profilePic.jpg"};
// below code to generate 100 random messages
	var objId=0;
	var tweets=new userMessageCollection();

	_.times(100,function(){
		objId=objId+1;
		var randomUserIndex=_.random(0, users.length-1);
		var randomMessageIndex=_.random(0, messages.length-1);
		var tweet=new userMessageModel({
			user:users[randomUserIndex],
			message:messages[randomMessageIndex],
			id:objId
		});
		tweets.add(tweet);
	});

new messagesView({collection:tweets});
new followListView({collection:users});
//onclick of tweet button by user
$('#tweet').click(function(event){
		var tempTweetObj=new Object();
		tempTweetObj.user=loginUser;
		var tempMessageObj=new Object();
		tempMessageObj.text=$('#tweetText').val();
		tempMessageObj.image="";
		tempTweetObj.message=tempMessageObj;
		tempTweetObj.id=tweets.length+1;
		tweets.unshift(tempTweetObj);
		new messagesView({collection:tweets});
});

//increment heart badge value by 1

function increment(glyphiconId){
	$('#'+glyphiconId+' > span').text(parseInt($('#'+glyphiconId+' > span').text())+1);
}