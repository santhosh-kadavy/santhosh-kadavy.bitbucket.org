var messagesView=Backbone.View.extend({
	el:'#tweetList',
	template:_.template($('#tweetsTemplate').html()),
	initialize:function(){
		this.$el.empty();
		this.render();
	},
	render:function(){
			return this.$el.append(this.template({tweets:this.collection.toJSON()}));
	}
});

var followListView=Backbone.View.extend({
	el:'#followList',
	template:_.template($('#followTemplate').html()),
	initialize:function(){
		this.$el.empty();
		this.render();
	},
	render:function(){
			return this.$el.append(this.template({followList:this.collection}));
	}
});