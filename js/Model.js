var userModel=Backbone.Model.extend({
	defaults:{
		name:'',
		gender:'',
		dp:''
	}
});

var messageModel=Backbone.Model.extend({
	defaults:{
		text:'',
		image:''
	}
});

var userMessageModel=Backbone.Model.extend({
	defaults:{
		user:new userModel(),
		message:new messageModel(),
		id:''
	}
});
var userCollection=Backbone.Collection.extend({
	model:userModel
})

var messageCollection=Backbone.Collection.extend({
	model:messageModel
})

var userMessageCollection=Backbone.Collection.extend({
	model:userMessageModel
})